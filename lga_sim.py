# lga_sim.py: the actual lga simulation
import numpy as np
import matplotlib.image as mpimg
import math as m
from random import randint
import sys, os, random
import time

from lga_params import *
import pyximport; pyximport.install()
import lga_update               # cython module

class LGA:
    def __init__(self, visualizer=None, dim=None):
        self.visualizer = visualizer
        self.regions = dict()        # dictionary of coordinates of interest
        self.stepCallbacks = list()
        self.plotlabel = "Statistics"
        if dim is not None:
            A = np.zeros(dim, dtype=int)
            self.setup(A)
            self.computeImg(self.A)
            
    def initFHPrule(self, col):
        for i in xrange(2**NBITS):
            col[i] = i
        #df = 0*ZZ + 0*BD # default
        df = 0*ZZ  # default

        # 2 particle collision rules
        # Rev 2! Meant to be reversible
        # Re-ordered to put inverse rules next to each other
        # 0 on input==>rotate clockwise
        # 1 on input==>rotate counter-clockwise
        col[EE + WW + df + 0*CC] = NW + SE + df + 1*CC # Clockwise
        col[NW + SE + df + 1*CC] = EE + WW + df + 0*CC # Counterclockwise

        col[NE + SW + df + 0*CC] = EE + WW + df + 1*CC # Clockwise
        col[EE + WW + df + 1*CC] = NE + SW + df + 0*CC # Counterclockwise

        col[NW + SE + df + 0*CC] = NE + SW + df + 1*CC # Clockwise
        col[NE + SW + df + 1*CC] = NW + SE + df + 0*CC # Counterclockwise

        # 3 particle collision rules
        col[NW + SW + EE + df + 0*CC] = NE + WW + SE + df + 0*CC
        col[NW + SW + EE + df + 1*CC] = NE + WW + SE + df + 1*CC

        col[NE + WW + SE + df + 0*CC] = NW + SW + EE + df + 0*CC
        col[NE + WW + SE + df + 1*CC] = NW + SW + EE + df + 1*CC

        # Boundary
        for i in xrange(2**NBITS): # For every possible cell state
            bdbits = i&BDMASK
            if (bdbits==REV): # Reversing boundary
                newi=0
                if (i & EE):
                    newi+=WW
                if (i & NE):
                    newi+=SW
                if (i & NW):
                    newi+=SE
                if (i & WW):
                    newi+=EE
                if (i & SW):
                    newi+=NE
                if (i & SE):
                    newi+=NW
                mask = ((2**NBITS)-1)-(EE+NE+NW+WW+SW+SE)
                col[i] = (i&mask)+newi
            if (bdbits==SRC):
                newi = EE # Hack for now...source always produces EE particules
                mask = ((2**NBITS)-1)-(EE+NE+NW+WW+SW+SE)
                col[i] = (i&mask)+newi
            if (bdbits==SNK):
                newi = 0 # Going to zero whatever particles are there...
                mask = ((2**NBITS)-1)-(EE+NE+NW+WW+SW+SE)
                col[i] = (i&mask)+newi
            # Reflecting Walls
            for wallang in range(MAXANG): # For each wall angle
                #wallnorm = (wallang+3)%MAXANG
                wallbits=WALLANG2BIT[wallang]
                if (bdbits==wallbits): # If this is the right entry for the wall we're considering...
                    newi = 0
                    for pd in range(6): # particle dir
                        if (i & (2**pd)):
                            pdw = 2*pd # particle dir using Wall angles
                            #print " wallang= ", wallang,
                            #print " pdw= ",pdw,
                            #print " MAXANG= ", MAXANG,
                            #print " ref= ", (2*(wallang-pdw))%MAXANG,
                            ref = (2*(wallang-pdw))%MAXANG
                            #print " PARTANG2BIT[ref]= ", PARTANG2BIT[ref] 
                            newi += PARTANG2BIT[ref]
                    mask = ((2**NBITS)-1)-(EE+NE+NW+WW+SW+SE)
                    col[i] = (i&mask)+newi

            # if (bdbits==EEEE): # EEEE wall
            #     newi=0
            #     if (i & NE):
            #         newi+=SW
            #     if (i & NW):
            #         newi+=SE
            #     if (i & SE):
            #         newi+=NW
            #     if (i & SW):
            #         newi+=NE
            #     mask = ((2**NBITS)-1)-(EE+NE+NW+WW+SW+SE)
            #     col[i] = (i&mask)+newi                
        return


    # A rule table whose purpose is to count bits.  So for example,
    # 0101,1010,0011,1100,1001 all map to 2 
    # 0111,1011,1101,1110 all map to 3, etc 
    def initCountrule(self, col):
        for i in xrange(2**NBITS):
            nparts = 0
            for b in xrange(7): # This should be 7 to count stationary; 6 for only moving
                if (i & (2**b)):
                    nparts += 1
            col[i] = nparts
        return

    # A rule table whose purpose is to estimate the pressure
    # It does this by computing the number of particles currently
    # in a boundary cell
    # This will incorrectly count collisions with internal boundaries 
    # as contributing to pressure.  Also, it doesn't account for 
    # particle direction
    def initPressurerule(self, col):
        for i in xrange(2**NBITS):
            nparts = 0
            col[i] = 0
            bdbits = i&BDMASK
            if (bdbits!=0): # For any boundary cell
                for b in xrange(7): # This should be 7 to count stationary; 6 for only moving
                    if (i & (2**b)):
                        nparts += 1
                col[i] = nparts
        return

    # This rule remaps the bits into an 8 bit range to make it
    # more easily displayable
    def initColorrule(self, col):
        print "Color rule: "
        for i in xrange(2**NBITS):
            nparts = 0
            for b in xrange(6):
                if (i & (2**b)):
                    nparts += 1
            col[i] = nparts # maps 0-7 to 0-7
            if (i & BDMASK):
                col[i]=col[i]+128;
        return


    # error: 521 (2 parts) == 512 + 8 + 1 -> 516 (1 part)  512+ 4
    # This is EENE wall, EE particle, WW particle --> NW particle
    def testrule(self,col):
        n=len(col)
        print "Testing rule table to make sure particles are conserved. "
        for i in xrange(n):
            cellin    = i
            cellout   = col[i]
            partsin   = cellin & PARTMASK
            partsout  = cellout & PARTMASK
            npartsin  = 0
            npartsout = 0
            for b in xrange(7):
                if (cellin & (2**b)):
                    npartsin += 1
                if (cellout & (2**b)):
                    npartsout += 1
            if (((i & SRC)==0) and((i & SNK)==0)): # SRC and SNK don't conserve, so don't complain on those
                if (npartsin != npartsout):
                    print "Rule error: ", cellin,
                    print " (",npartsin, " particles in) -->",
                    print cellout,
                    print " (",npartsout, " particles out)"

    def clear(self, A):
        sh = A.shape
        nr = sh[0]
        nc = sh[1]
        for i in xrange(nr):
          for j in xrange(nc):
              A[i,j] = 0
        return

    # arect is a tuple representing a rectangle
    def RectFill(self, p, arect):
        A  = self.A
        sh = A.shape
        nr = sh[0]
        nc = sh[1]
        r1,c1,r2,c2 = arect
        for i in xrange(r1,r2):
            for j in xrange(c1,c2):
              A[i,j] = 0
              for b in xrange(7):
                  if random.random() < p:
                      A[i,j] += 2**b
        return

    # arect is a tuple representing a rectangle
    # A should be initialized to all zeros once before this is called
    def RectNFill(self, p, arect):
        A           = self.A
        sh          = A.shape
        nr          = sh[0]
        nc          = sh[1]
        r1,c1,r2,c2 = arect
        vol         = (r2-r1)*(c2-c1)
        N           = p*vol*6
        nparts      = 0
        while (nparts<N):
            i = randint(r1,r2-1)
            j = randint(c1,c2-1)
            b = randint(0,5) # change this to 6 if you want stationary particles too
            if ((A[i,j] & 2**b)==0): # if randomly chosen site is not already occupied
                A[i,j] += 2**b
                nparts+=1
        print "Initializing rect ", arect
        print "vol= ", vol, "  N=",N,"  nparts= ",nparts
        return

    
    def RectInit(self, p,r1,c1,r2,c2):
        A  = self.A
        for i in xrange(r1,r2+1):
            for j in xrange(c1,c2+1):
              A[i,j] &= ~0b111111 # clear particle bits
              for b in xrange(6):
                  if random.random() < p:
                      A[i,j] += 2**b
        return


    def RandInit(self, p, nr, nc):
        A  = self.A
        for i in xrange(nr):
          for j in xrange(nc):
              A[i,j] &= ~0b111111 # clear particle bits
              for b in xrange(6):
                  if random.random() < p:
                      A[i,j] += 2**b
        return


    # Set initialConditions
    def SparseInit(self):
        A        = self.A
        A[14,1]  = EE+NE+NW+WW+SW+SE
        A[14,7]  = EE+NE+NW+WW+SW+SE
        A[15,13] = EE+NE+NW+WW+SW+SE
        A[2,4]   = EE+NE+NW+WW+SW+SE
        A[2,5]   = EE+NE+NW+WW+SW+SE
        A[6,5]   = BD
        return

    def DrawWalls(self):
        A         = self.A
        sh        = A.shape
        nr        = sh[0]
        nc        = sh[1]
        A[0,:]    = REV
        A[nr-1,:] = REV
        A[:,0]    = REV
        A[:,nc-1] = REV
        return

    def DrawDoubleWalls(self):
        A         = self.A
        sh        = A.shape
        nr        = sh[0]
        nc        = sh[1]
        A[0,:]    = REV
        A[1,:]    = REV
        A[nr-2,:] = REV
        A[nr-1,:] = REV
        A[:,0]    = REV
        A[:,1]    = REV
        A[:,nc-2] = REV
        A[:,nc-1] = REV
        return

    def DrawLine(self, r1,c1,r2,c2):
        A      = self.A
        sh     = A.shape
        nr     = sh[0]
        nc     = sh[1]
        length = ((r2-r1)**2 + (c2-c1)**2)**0.5
        for t in np.arange(0.0,length-1.0,0.1):
            i = r1+int(t*(r2-r1)/(length-1))
            j = c1+int(t*(c2-c1)/(length-1))
            A[i,j] = REV
        return
    
    def DrawHorzMembrane(self, radius):
        A           = self.A
        sh          = A.shape
        nr          = sh[0]
        nc          = sh[1]
        midway      = int(nr/2.0)
        A[midway,:] = REV
        for c in range(-radius,radius):
            A[midway,int(nc/2.0)+c]= 0
        return

    def DrawVertMembrane(self, radius):
        A           = self.A
        sh          = A.shape
        nr          = sh[0]
        nc          = sh[1]
        midway      = int(nc/2.0)
        A[:,midway] = REV
        for r in range(-radius,radius):
            A[int(nr/2.0)+r,midway]= 0
        return

    def printhexarr(self,C,ndig): # ndig == number of digits (to print, per cell)
        sh = C.shape
        nr = sh[0]
        nc = sh[1]
        for i in range(0,nr,2):
            for j in range(nc):
                if ((C[i,j]&BDMASK)!=0): # we are in a boundary cell
                    if ((C[i,j]&MOVINGMASK)!=0):

                        tok="."
                    else:
                        tok = "*"
                else:
                    tok = repr(C[i,j])
                tok = tok.rjust(ndig)
                print tok, 
            print
            print
            i=i+1
            print " ",
            for j in range(nc):
                if ((C[i,j]&BDMASK)!=0): # we are in a boundary cell
                    if ((C[i,j]&MOVINGMASK)!=0):
                        tok="."
                    else:
                        tok = "*"
                else:
                    tok = repr(C[i,j])
                tok = tok.rjust(ndig)
                print tok, 
            print
            print

    def printrawhexarr(self,C,ndig): # ndig == number of digits (to print, per cell)
        sh = C.shape
        nr = sh[0]
        nc = sh[1]
        for i in range(0,nr,2):
            for j in range(nc):
                tok = repr(C[i,j])
                tok = tok.rjust(ndig)
                print tok, 
            print
            print
            i=i+1
            print " ",
            for j in range(nc):
                tok = repr(C[i,j])
                tok = tok.rjust(ndig)
                print tok, 
            print
            print


    # Propagate entire array
    def propagate(self,A,B):
        lga_update.evenprop(A,B)
        lga_update.oddprop(A,B)
        return


    def print_state(self):
        print
        print "B= "
        self.printhexarr(self.B,3)
        print
        print "t= ", self.t
        print "A= "
        self.printhexarr(self.A,3)
        print
        print "t= ", self.t
        print "Raw A= "
        self.printrawhexarr(self.A,3)
        return

    
    def onestat(self):
        sumdense=0.0
        for s in self.statslist:
            adensity = self.rectcount(self.B,self.Countrule,s)
            print adensity, " ",
            sumdense += adensity
        print "  sum: ", sumdense
        print
        return

    
    def reverse(self):
        #global dt
        print "Reversal at time t= ", self.t
        self.dt = (self.dt*(-1)) # Reverse the direction of time!
        sh      = self.A.shape
        nr      = sh[0]
        nc      = sh[1]
        for i in xrange(nr):
          for j in xrange(nc):
            newp = 0
            oldp = self.A[i,j] & MOVINGMASK # mask off the moving particles
            if (oldp & EE):
                newp+=WW
            if (oldp & NE):
                newp+=SW
            if (oldp & NW):
                newp+=SE
            if (oldp & WW):
                newp+=EE
            if (oldp & SW):
                newp+=NE
            if (oldp & SE):
                newp+=NW
            mask = ((2**NBITS)-1)-MOVINGMASK # mask for everything other than the moving particles
            self.B[i,j] = (self.A[i,j]&mask)+newp                
        lga_update.collide(self.B,self.FHP) #!! This fixes the reverse problem!!!
        lga_update.copy(self.B, self.A)
        return

    
    def numParticles(self):
        crule    = self.Countrule
        E        = self.A
        sh       = E.shape
        nr       = sh[0]
        nc       = sh[1]
        thecount = 0
        for i in xrange(nr):
          for j in xrange(nc):
            thecount += crule[E[i,j]]
        return thecount

    
    def addRegion(self, idNum, x1, y1, x2, y2):
        coords = []
        for y in xrange(y1,y2+1):
            for x in xrange(x1, x2+1):
                coords.append((y,x))
                self.regions_img[y,x] = idNum
        self.regions[idNum] = np.array(coords)
        self.stattimeseries[idNum] = list()
        
    # E: the universe
    # t: current time step (scalar)
    # sl: statslist---list of statistics to compute
    # tl: timelist---list of time values
    # sts: stattimeseries a list of timeseries
    def computeStats(self,E,t,sl,tl,sts):
        tl.append(t)
        count = 0
        for reg_id, region in self.regions.items():
            for i in xrange(region.shape[0]):
                y,x = region[i]
                count += self.Countrule[E[y,x]]
            sts[reg_id].append(float(count) / region.shape[0])
        '''
        for s in sl:
            adensity = self.rectdensity(E,self.Countrule,s)
            sts[i].append(adensity)
            i+=1
        '''
        return

    ########################################################################
        
    def setup(self, E):
        self.t          = 0
        self.dt         = +1
        nr, nc          = E.shape
        self.A          = E
        self.B          = self.A.copy()
        self.initialLGA = self.A.copy() # Save this state for later resets

        # Create an "image" of the region areas
        self.regions_img = np.zeros(self.A.shape)
        for reg_id, region in self.regions.items():
            for y,x in region:
                self.regions_img[y,x] = int(reg_id) / 255.0
                    
            
        self.img = np.zeros((nr,nc),dtype=int)
        self.bdry = np.zeros((nr,nc),dtype=int) # Separate buffer holding the boundary conditions

        self.FHP = np.zeros(2**NBITS,dtype=int) # allocate array to hold FHP rule table
        self.initFHPrule(self.FHP) # initialize FHP rule

        self.Countrule = np.zeros(2**NBITS,dtype=int) # allocate array to hold Count rule table
        self.initCountrule(self.Countrule) # initialize Count rule

        self.Pressurerule = np.zeros(2**NBITS,dtype=int) # allocate array to hold Pressure rule table
        self.initPressurerule(self.Pressurerule) # initialize Pressure rule

        self.Colorrule = np.zeros(2**NBITS,dtype=int) # allocate array to hold Color rule table
        self.initColorrule(self.Colorrule) # initialize Color rule

        self.testrule(self.FHP) # test FHP rule to see if it always conserves particles


        # Set up stats
        # General stats set up
        #self.statslist = list() # list of the stats to compute (rects in which to find density)
        #self.setup_stats(self.statslist)

        # Allocate lists to hold various time series
        self.timelist = list()       # list of times
        self.stattimeseries = dict() # to hold the simulation results
        for s in self.regions.keys():     # One column vector for each stat we're computing
            self.stattimeseries[s] = list()

        self.update_time = 0      # how long it takes to perform an update step
        self.steps_per_stat = 10  # number steps between stat computations
        self.stat_step = 0
        print "Initial particle count: ", self.numParticles()
        print "Finished LGA set up"
        return

    def reset(self):
        self.setup(self.initialLGA)
        
    def loadImage(self, filename):
        img = mpimg.imread(filename)
        if len(img.shape) == 2:
            print "Please use a color image."
        else:
            self.plotlabel = "Statistics (%s)"%filename.split('/')[-1]
            regions = {}
            A = np.zeros(img.shape[:2], dtype=int)
            for y in xrange(img.shape[0]):
                for x in xrange(img.shape[1]):
                    if img[y,x,0] > 0:
                        # RED is a wall
                        A[y,x] = REV
                    if img[y,x,2] > 0:
                        # BLUE is a particle
                        val = int(img[y,x,2] * 255) >> 2
                        A[y,x] += val
                    if img[y,x,1] > 0:
                        # GREEN is a statistically significant region
                        reg_id = int(img[y,x,1] * 255)
                        if reg_id not in regions:
                           regions[reg_id] = list()
                        regions[reg_id].append((y,x))

            # Convert to numpy arrays and store for later
            self.regions = {}
            for reg_id, region in regions.items():
                self.regions[reg_id] = np.array(region)

            self.setup(A)
            self.computeImg(self.A)

            if self.visualizer is not None:
                self.visualizer.lgaChanged()

    
    def step(self, print_stats = False):
        now = time.time()
        self.t += self.dt
        self.propagate(self.A,self.B)
        lga_update.collide(self.B,self.FHP)

        for cb in self.stepCallbacks:
            cb()

        #if self.stat_step % self.steps_per_stat == 0:
        #    self.computeStats(self.B,self.t,self.statslist,self.timelist,self.stattimeseries)
        self.stat_step += 1
        
        # Swap so updated is in A
        tmp = self.A
        self.A = self.B
        self.B = tmp

        if print_stats:
            self.onestat()
            
        self.update_time = time.time() - now
        return

    
    def computeImg(self, A):
        sh=A.shape
        nr=sh[0]
        nc=sh[1]
        for i in xrange(nr):
            for j in xrange(nc):
                self.img[i,j] = self.Colorrule[A[i,j]]
        return
