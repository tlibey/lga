import cython
#from LGAParams import *
cimport numpy as np

# Declare types for the globals imported from LGAParams
cdef int  nr, nc, MAXT, MD, NBITS
cdef int  EE,NE,NW,WW,SW,SE,ZZ,CC,BD0,BD1,BD2,BD3,REV 
cdef int  MOVINGMASK   # Mask for detecting moving particles
cdef int  PARTMASK     # Mask for detecting any particle (including stationary)
cdef int  BDMASK     # Mask for detecting boundary
cdef int  EE_E_j,NE_E_j,NW_E_j,WW_E_j,SW_E_j,SE_E_j    
cdef int  EE_E_i,NE_E_i,NW_E_i,WW_E_i,SW_E_i,SE_E_i 
cdef int  EE_O_j,NE_O_j,NW_O_j,WW_O_j,SW_O_j,SE_O_j
cdef int  EE_O_i,NE_O_i,NW_O_i,WW_O_i,SW_O_i,SE_O_i

from lga_params import *

@cython.boundscheck(False)
def evenprop(np.ndarray[np.int64_t, ndim=2, mode="c"] A not None,
             np.ndarray[np.int64_t, ndim=2, mode="c"] B not None):
    cdef int i, j, nr, nc
    sh=A.shape
    nr=sh[0]
    nc=sh[1]
    for i in range(0,nr-1,2): # NB: -ve indices wrap the right way!
      for j in range(nc-1):          
          B[i,j] = (( 1&A[i+EE_E_i,j+EE_E_j]) + # The paren around each term is needed... + has 
                    ( 2&A[i+NE_E_i,j+NE_E_j]) + # higher precedence than & !!
                    ( 4&A[i+NW_E_i,j+NW_E_j]) +
                    ( 8&A[i+WW_E_i,j+WW_E_j]) +
                    (16&A[i+SW_E_i,j+SW_E_j]) +
                    (32&A[i+SE_E_i,j+SE_E_j]) +
                    ((ZZ+BDMASK+CC)&A[i+0     ,j+0] ))
      j=nc-1 # special case for rightmost column
      B[i,j] = (( 1&A[i+EE_E_i,(j+EE_E_j)%(nc)]) + # The paren around each term is needed... + has 
                ( 2&A[i+NE_E_i,(j+NE_E_j)%(nc)]) + # higher precedence than & !!
                ( 4&A[i+NW_E_i,(j+NW_E_j)%(nc)]) +
                ( 8&A[i+WW_E_i,(j+WW_E_j)%(nc)]) +
                (16&A[i+SW_E_i,(j+SW_E_j)%(nc)]) +
                (32&A[i+SE_E_i,(j+SE_E_j)%(nc)]) +
                ((ZZ+BDMASK+CC)&A[i+0     ,j+0] ))
    i=nr-1 # special case for bottom row
    for j in range(nc-1):
      B[i,j] = (( 1&A[(i+EE_E_i)%(nr),j+EE_E_j]) + # The paren around each term is needed... + has 
                ( 2&A[(i+NE_E_i)%(nr),j+NE_E_j]) + # higher precedence than & !!
                ( 4&A[(i+NW_E_i)%(nr),j+NW_E_j]) +
                ( 8&A[(i+WW_E_i)%(nr),j+WW_E_j]) +
                (16&A[(i+SW_E_i)%(nr),j+SW_E_j]) +
                (32&A[(i+SE_E_i)%(nr),j+SE_E_j]) +
                ((ZZ+BDMASK+CC)&A[i+0     ,j+0] ))
    j=nc-1 # special special case: bottom right node
    B[i,j] = (( 1&A[(i+EE_E_i)%(nr),(j+EE_E_j)%(nc)]) + # The paren around each term is needed... + has 
              ( 2&A[(i+NE_E_i)%(nr),(j+NE_E_j)%(nc)]) + # higher precedence than & !!
              ( 4&A[(i+NW_E_i)%(nr),(j+NW_E_j)%(nc)]) +
              ( 8&A[(i+WW_E_i)%(nr),(j+WW_E_j)%(nc)]) +
              (16&A[(i+SW_E_i)%(nr),(j+SW_E_j)%(nc)]) +
              (32&A[(i+SE_E_i)%(nr),(j+SE_E_j)%(nc)]) +
              ((ZZ+BDMASK+CC)&A[i+0     ,j+0] ))
    return


# Fill in odd rows by propagating
@cython.boundscheck(False)
def oddprop(np.ndarray[np.int64_t, ndim=2, mode="c"] A not None,
            np.ndarray[np.int64_t, ndim=2, mode="c"] B not None):
    cdef int i, j, nr, nc
    sh=A.shape
    nr=sh[0]
    nc=sh[1]
    for i in range(1,nr-1,2): # NB: -ve indices wrap the right way!
      for j in range(nc-1):
        B[i,j] = (( 1&A[i+EE_O_i,j+EE_O_j]) + # The paren around each term is needed... + has 
                  ( 2&A[i+NE_O_i,j+NE_O_j]) + # higher precedence than & !!
                  ( 4&A[i+NW_O_i,j+NW_O_j]) +
                  ( 8&A[i+WW_O_i,j+WW_O_j]) +
                  (16&A[i+SW_O_i,j+SW_O_j]) +
                  (32&A[i+SE_O_i,j+SE_O_j]) +
                  ((ZZ+BDMASK+CC)&A[i+0     ,j+0] ))
      j=nc-1 # special case for rightmost column
      B[i,j] = (( 1&A[i+EE_O_i,(j+EE_O_j)%(nc)]) + # The paren around each term is needed... + has 
                ( 2&A[i+NE_O_i,(j+NE_O_j)%(nc)]) + # higher precedence than & !!
                ( 4&A[i+NW_O_i,(j+NW_O_j)%(nc)]) +
                ( 8&A[i+WW_O_i,(j+WW_O_j)%(nc)]) +
                (16&A[i+SW_O_i,(j+SW_O_j)%(nc)]) +
                (32&A[i+SE_O_i,(j+SE_O_j)%(nc)]) +
                ((ZZ+BDMASK+CC)&A[i+0     ,j+0] ))
      
    i=nr-1 # special case for bottom row
    for j in range(nc-1):
      B[i,j] = (( 1&A[(i+EE_O_i)%(nr),j+EE_O_j]) + # The paren around each term is needed... + has 
                ( 2&A[(i+NE_O_i)%(nr),j+NE_O_j]) + # higher precedence than & !!
                ( 4&A[(i+NW_O_i)%(nr),j+NW_O_j]) +
                ( 8&A[(i+WW_O_i)%(nr),j+WW_O_j]) +
                (16&A[(i+SW_O_i)%(nr),j+SW_O_j]) +
                (32&A[(i+SE_O_i)%(nr),j+SE_O_j]) +
                ((ZZ+BDMASK+CC)&A[i+0     ,j+0] ))
    j=nc-1 # special special case: bottom right node
    B[i,j] = (( 1&A[(i+EE_O_i)%(nr),(j+EE_O_j)%(nc)]) + # The paren around each term is needed... + has 
              ( 2&A[(i+NE_O_i)%(nr),(j+NE_O_j)%(nc)]) + # higher precedence than & !!
              ( 4&A[(i+NW_O_i)%(nr),(j+NW_O_j)%(nc)]) +
              ( 8&A[(i+WW_O_i)%(nr),(j+WW_O_j)%(nc)]) +
              (16&A[(i+SW_O_i)%(nr),(j+SW_O_j)%(nc)]) +
              (32&A[(i+SE_O_i)%(nr),(j+SE_O_j)%(nc)]) +
              ((ZZ+BDMASK+CC)&A[i+0     ,j+0] ))
    return

@cython.boundscheck(False)
def collide(np.ndarray[np.int64_t, ndim=2, mode="c"] C not None,
            np.ndarray[np.int64_t, ndim=1, mode="c"] rule not None):
    cdef int nr, nc, i, j
    sh=C.shape
    nr=sh[0]
    nc=sh[1]
    for i in range(1,nr):
      for j in range(nc):
          C[i,j]=rule[C[i,j]]
    return


# Copy from C into D
@cython.boundscheck(False)
def copy(np.ndarray[np.int64_t, ndim=2, mode="c"] C not None,
         np.ndarray[np.int64_t, ndim=2, mode="c"] D not None):
    cdef int nr, nc, i, j
    sh=C.shape
    nr=sh[0]
    nc=sh[1]

    for i in range(nr):
      for j in range(nc):
        D[i,j] = C[i,j]
    return
