# lga_experiment.py: Base Experiment class...derive your experiments from this
import time
import lga_sim
import numpy as np
import math

class EmptyVis:
    def noop(self):
        print "No visualizer assigned."
        return
    
    def __getattr__(self, name):
        return self.noop
    
class Experiment(object):
    def __init__(self, visualizer = None):
        self.name = "Some Experiment"
        self.stat_step = 0
        self.steps_per_stat = 10
        self.timelist = list()       # list of times
        self.stattimeseries = dict() # to hold the simulation results
        self.last_update = time.time()
        self.update_hertz = 10.0
        self.currentStatFn = self.stat_density # sane default
        if visualizer is None:
            self.visualizer = EmptyVis()
        else:
            self.visualizer = visualizer
        return

    def loadImageLGA(self, filename):
        self.lga = lga_sim.LGA(visualizer=self.visualizer)
        self.lga.loadImage(filename)
        self._genericSetup()

    def loadBlankLGA(self, dim):
        self.lga = lga_sim.LGA(visualizer=self.visualizer, dim=dim)
        self._genericSetup()
        
    def _genericSetup(self):
        self.lga.stepCallbacks.append(self.computeStats)
        self.timelist = list()       # list of times
        self.stattimeseries = dict() # to hold the simulation results
        for s in self.lga.regions.keys():     # One column vector for each stat we're computing
            self.stattimeseries[s] = list()
        self.visualizer.lgaChanged()

    def setup(self):
        raise NotImplementedError("Must implement setup")
        return
    
    def computeStats(self):
        self.stat_step += 1
        if self.stat_step % self.steps_per_stat != 0:
            return
        
        self.lga.timelist.append(self.lga.t)
        sts = self.lga.stattimeseries
        for reg_id, region in self.lga.regions.items():
                sts[reg_id].append(self.currentStatFn(region))
       
        return
        
    def step(self):
        self.lga.step()
        return

    def start(self):
        raise NotImplementedError("Must implement start")
        return

    def reset(self):
        self.lga.reset()
        return

    def saveImg(self, filename):
        self.lga.computeImg(self.lga.A)
        self.visualizer.ui_redraw()
        self.visualizer.fig.savefig(filename)
        print "Image saved to", filename

    ######################################################################
    ## COMMON STATISTICS
    ######################################################################
    
    def stat_density(self, region, verbose=False):
        '''
        Calculates the density in a pre-defined region
        region - an Nx2 np.array of y,x coordinates to consider
        '''
        crule = self.lga.Countrule
        count = 0
        E     = self.lga.A
        for y,x in region:
            count += crule[E[y,x]]
        density = float(count) / region.shape[0]
        
        if verbose:
            print "vol= ", region.shape[0]
            print "count= ", count
            print "density= ", density

        return density

    
    def rectcount(self, arect):
        '''
        Counts the number of particles in an arbitray rectangle.
        arect - [y1,x1, y2,x2]
        '''
        crule       = self.lga.Countrule
        E           = self.lga.A
        sh          = E.shape
        nr          = sh[0]
        nc          = sh[1]
        thecount    = 0
        r1,c2,r2,c2 = arect
        for i in xrange(r1,r2):
          for j in xrange(c1,c2):
            thecount += crule[E[i,j]]
        return thecount


    def rectvol(self, arect):
        '''
        Returns the volume of a rectangle.
        arect - [y1,x1, y2,x2]
        '''
        return math.abs((x1-x2) * (y1-y2))


    def rectdensity(self, arect):
        '''
        Calculates the density in an arbitrary rectangle.
        arect - [y1,x1, y2,x2]
        '''
        density = self.rectcount(arect)/self.rectvol(arect)
        return density

    def stat_pressure(self, region, verbose=False):
        '''
        Estimates pressure by counting number of particles in boundary cells
        in a given region
        '''
        prule = self.lga.Pressurerule
        p = 0 # pressure
        E     = self.lga.A
        for y,x in region:
            p += prule[E[y,x]]
        
        if verbose:
            print "vol= ", region.shape[0]
        return p
